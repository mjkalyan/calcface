(in-package :cl-user)
(defpackage calcface.gui
  (:use :cl+qt)
  (:export :run))
(in-package :calcface.gui)
(in-readtable :qtools)

;;; Widgets

(define-widget calcface-window (QWidget) ())

(define-subwidget (calcface-window input-pane) (q+:make-qlineedit calcface-window)
  (setf (q+:placeholder-text input-pane) "Start calculating!"))

(define-subwidget (calcface-window history-pane) (q+:make-qtextedit calcface-window)
  (setf (q+:read-only history-pane) t)
  (q+:set-text history-pane "history goes here"))

(define-subwidget (calcface-window stack-pane) (q+:make-qtextedit calcface-window)
  (setf (q+:read-only stack-pane) t)
  (q+:set-text stack-pane "stack output goes here"))

;;; Layouts

;; for displaying the stack/history
(define-subwidget (calcface-window read-only-layout) (q+:make-qhboxlayout calcface-window)
  (q+:add-widget read-only-layout stack-pane)
  (q+:add-widget read-only-layout history-pane))

;; main layout
(define-subwidget (calcface-window main-layout) (q+:make-qvboxlayout calcface-window)
  (q+:add-layout main-layout read-only-layout)
  (q+:add-widget main-layout input-pane))

;;; Run the application

(defun run ()
  (with-main-window (window 'calcface-window)))
