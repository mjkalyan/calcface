(in-package :cl-user)
(defpackage calcface
  (:use :cl
        :calcface.operations
        :calcface.gui)
  (:export :main))
(in-package :calcface)

(defun main ()
  (run))
