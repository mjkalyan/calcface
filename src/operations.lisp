(in-package :cl-user)
(defpackage calcface.operations
  (:use :cl))
(in-package :calcface.operations)

(eval-when (:compile-toplevel :load-toplevel :execute)
           (defparameter *inflation* 0.03))

(defmacro -inflation (inflation years fn &rest args)
  "Apply the INFLATION rate to the result of passing ARGS the function FN assuming some number of
YEARS has passed."
  `(* (funcall ,fn ,@args)
      (expt ,(- 1 inflation) ,years)))

(defmacro defmath-periodic (name args period-type &body body)
  "Define a math operation, NAME, which takes a list of ARGS and a PERIOD-TYPE (which is a symbol
like years or months to use in BODY).

The associated *-WITH-INFLATION function is also defined."
  `(progn
     (defun ,name (,@args ,period-type)
       ,@body)
     (defun ,(intern (concatenate 'string (symbol-name name) "-WITH-INFLATION"))
         (,@args ,period-type)
       (-inflation ,*inflation* ,period-type #',name ,@args ,period-type))))

(defmath-periodic appreciation-minus-expenses (initial-amount yearly-expenses appreciation) years
  (if (<= years 0)
      initial-amount
      (loop repeat years
            for running-total = (* (- initial-amount yearly-expenses) appreciation)
              then (* (- running-total yearly-expenses) appreciation)
            finally (return running-total))))

(defmath-periodic average-dividend-earnings (initial-amount
					     %appreciation
					     %dividends
					     yearly-expenses) years
  (if (<= years 0)
      0
      (loop repeat years
	    for running-balance = (* (- initial-amount yearly-expenses) %appreciation)
	      then (* (+ running-balance (- (* running-balance %dividends) yearly-expenses))
	       	      %appreciation)
	    sum (* running-balance %dividends) into total-dividends
	    finally (return (/ total-dividends years)))))

(defun calculate-income-tax (income &key country province marginal-tax-rates-list)
  (let* ((marginal-tax-rates-list
	   (unless marginal-tax-rates-list (gethash country (marginal-rates-table))))
	 (margin-threshold (first (car marginal-tax-rates-list)))
	 (marginal-rate (second (car marginal-tax-rates-list))))
    (if (or (eq margin-threshold 'inf) (<= income margin-threshold))
	(* marginal-rate income)
	;; the recursive sum of the tax at each margin
	(+ (* margin-threshold marginal-rate)
	   (calculate-income-tax (- income margin-threshold)
				 :country country
				 :province province
				 :marginal-tax-rates-list (cdr marginal-tax-rates-list))))))

(defun marginal-rates-table ()
  "Return the income tax table for different countries & provinces/states."
  (let ((h (make-hash-table)))
    (setf (gethash 'canada h)
	  '((50197 0.15) (100392 0.205) (155265 0.26) (221708 0.29) ('inf 0.33)))
    (setf (gethash 'alberta h)
	  '((131220 0.10) (157464 0.12) (209952 0.13) (314928 0.14) ('inf 0.15)))
    h))
