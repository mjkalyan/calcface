(defsystem "calcface"
  :author "James Kalyan <contrib_x@protonmail.com>"
  :maintainer "James Kalyan <contrib_x@protonmail.com>"
  :license "GPL-3+"
  :homepage "https://gitlab.com/mjkalyan/calcface"
  :version "0.1.0"
  :depends-on (:qtools :qtcore :qtgui)
  :components ((:module "src"
                :components
                ((:file "calcface" :depends-on ("operations" "gui"))
                 (:file "operations")
                 (:file "gui"))))
  :description "An interface to math functions similar to emacs-calc"
  :in-order-to ((test-op (test-op "calcface-test"))))
